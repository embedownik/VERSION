/*
 * version.h
 *
 *      Author: embedownik
 *
 *  Simple device info and firmware versioning interface.
 */

#ifndef VERSION_VERSION_INC_VERSION_H_
#define VERSION_VERSION_INC_VERSION_H_

#include <stdint.h>

typedef struct
{
    uint8_t MAJOR;
    uint8_t MINOR;
    uint8_t PATCH;
}version_version_s;

typedef struct
{
    version_version_s versionNumber;
    const char *compilationDate;      /* example: 'Jul 10 2015' */
    const char *deviceInfoStr1;       /* Info about device as string field 1 */
    const char *deviceInfoStr2;       /* Info about device as string field 2 */
    const char *commitSHA;            /* by default SHA1 is 40B long (160bits). For example: 557db03de997c86a4a028e1ebd3a1ceb225be238 */
    const char *branchName;           /* git branch name */
}version_all_s;

version_all_s version_GetVersionFull(void);

version_version_s version_GetVersion(void);

#endif
