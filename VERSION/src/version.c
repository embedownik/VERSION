#include <version.h>

#include <versionConfig.h>

static const char compilationDateStr[] = __DATE__;

static const char deviceInfoStr1[] = VERSION_DEF_DEVICE_INFO1_STR;
static const char deviceInfoStr2[] = VERSION_DEF_DEVICE_INFO2_STR;

static const char commitSHA[]  = VERSION_DEF_COMMIT_SHA;
static const char branchName[] = VERSION_DEF_BRANCH_NAME;

static const version_all_s version =
{
    .versionNumber =
    {
        .MAJOR = VERSION_DEF_VERSION_MAJOR,
        .MINOR = VERSION_DEF_VERSION_MINOR,
        .PATCH = VERSION_DEF_VERSION_PATCH,
    },
    .deviceInfoStr1  = deviceInfoStr1,
    .deviceInfoStr2  = deviceInfoStr2,
    .commitSHA       = commitSHA,
    .compilationDate = compilationDateStr,
    .branchName      = branchName,
};

version_all_s version_GetVersionFull(void)
{
    return version;
}

version_version_s version_GetVersion(void)
{
    return version.versionNumber;
}
