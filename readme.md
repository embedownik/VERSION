# VERSION

Simple module for firmware versioning - just very basic features.

Resourses about versioning:

- https://embeddedartistry.com/blog/2016/12/21/giving-your-firmware-build-a-version/
- https://semver.org/lang/pl/

# Usage

Create in your project file "versionConfig.h" with necessary defines (configuration is in separate file per project).


Example of "versionConfig" file:

```c
#define VERSION_DEF_VERSION_MAJOR (0U)
#define VERSION_DEF_VERSION_MINOR (1U)
#define VERSION_DEF_VERSION_PATCH (0U)

#define VERSION_DEF_DEVICE_INFO1_STR "Example dev 1"
#define VERSION_DEF_DEVICE_INFO2_STR "XYZ company"
#define VERSION_DEF_COMMIT_SHA       "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

```
